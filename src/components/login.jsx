import "./login.css";

function Login(){
  return(
    <div>
        <span className="header-ribbon"/>
  <div className="container-fluid">
    <div className="row">
        <div className="col-md-4 col-md-offset-4 col-centered">
            <div className="login-panel">
                <h4 className="login-panel-title">Sitename</h4>
                <p className="login-panel-tagline">Sitename helps you connect and share with the people in your life.</p>
                <div className="login-panel-section">
                    <div className="form-group">
                        <div className="input-group margin-bottom-sm">
                            <span className="input-group-addon"><i className="fa fa-envelope-o fa-fw" aria-hidden="true"></i></span>
                            <input className="form-control" type="text" placeholder="Email address"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-addon"><i className="fa fa-key fa-fw" aria-hidden="true"></i></span>
                            <input className="form-control" type="password" placeholder="Password"/>
                        </div>
                    </div>
                    <div className="checkbox checkbox-circle checkbox-success checkbox-small">
                        <input type="checkbox" id="checkbox1"/>
                        <label for="checkbox1">Keep me logged in</label>
                        <a href="#" className="pull-right">Forget your password?</a>
                    </div>
                </div>
                <div className="login-panel-section">
                    <button type="submit" className="btn btn-default"><i className="fa fa-sign-in fa-fw" aria-hidden="true"></i> Login</button> | <a href="#">Create Your Account</a>
                </div>
            </div>
        </div>
      </div>
    </div>

    </div>
  )
}

export default Login;