import "./portfolio.css";
import Cars1 from "./images/cars1.jpg";
import Cars2 from "./images/cars2.jpg";
import Cars3 from "./images/cars3.jpg";
import Girl from "./images/girl.jpg";
import Lights from "./images/lights.jpg";
import Nature from "./images/nature.jpg";
import Woman from "./images/woman.jpg";
import Mountain from "./images/mountains.jpg";
import Man from "./images/man.jpg";

function Portfolio() {
  return (
    <div className="main">
      <Header />

      <Gallery />
    </div>
  );
}

function buttonContainer() {
  return (
    <div id="myBtnContainer">
      <button className="btn active" onclick={filterSelection("all")}>
        Show all
      </button>
      <button className="btn" onclick={filterSelection("nature")}>
        Nature
      </button>
      <button className="btn" onclick={filterSelection("cars")}>
        Cars
      </button>
      <button className="btn" onclick={filterSelection("people")}>
        People
      </button>
    </div>
  );
}
function Header() {
  return (
    <div>
      <h1>MYLOGO.COM</h1>
      <hr />

      <h2>PORTFOLIO</h2>
    </div>
  );
}
function Gallery() {
  return (
    <div>
      <Gallerycolumn img={Mountain} name="Mountains" />
      <Gallerycolumn img={Lights} name="Lights" />
      <Gallerycolumn img={Nature} name="Nature" />
      <Gallerycolumn img={Cars1} name="Retro" />
      <Gallerycolumn img={Cars2} name="Fast" />
      <Gallerycolumn img={Cars3} name="Classic" />
      <Gallerycolumn img={Girl} name="Girl" />
      <Gallerycolumn img={Man} name="Man" />
      <Gallerycolumn img={Woman} name="Woman" />
    </div>
  );
}

function Gallerycolumn(Props) {
  return (
    <div className="column ">
      <div className="content">
        <img src={Props.img} alt={Props.name} style={{ width: "100%" }} />
        <h4>{Props.name}</h4>
        <p>Lorem ipsum dolor..</p>
      </div>
    </div>
  );
}
export default Portfolio;

/* <body>

<!-- MAIN (Center website) -->
<div className="main">

<h1>MYLOGO.COM</h1>
<hr>

<h2>PORTFOLIO</h2>

<div id="myBtnContainer">
  <button className="btn active" onclick="filterSelection('all')"> Show all</button>
  <button className="btn" onclick="filterSelection('nature')"> Nature</button>
  <button className="btn" onclick="filterSelection('cars')"> Cars</button>
  <button className="btn" onclick="filterSelection('people')"> People</button>
</div>

<!-- Portfolio Gallery Grid -->
<div className="row">
  <div className="column nature">
    <div className="content">
      <img src="/w3images/mountains.jpg" alt="Mountains" style="width:100%">
      <h4>Mountains</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column nature">
    <div className="content">
    <img src="/w3images/lights.jpg" alt="Lights" style="width:100%">
      <h4>Lights</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column nature">
    <div className="content">
    <img src="/w3images/nature.jpg" alt="Nature" style="width:100%">
      <h4>Forest</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  
  <div className="column cars">
    <div className="content">
      <img src="/w3images/cars1.jpg" alt="Car" style="width:100%">
      <h4>Retro</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column cars">
    <div className="content">
    <img src="/w3images/cars2.jpg" alt="Car" style="width:100%">
      <h4>Fast</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column cars">
    <div className="content">
    <img src="/w3images/cars3.jpg" alt="Car" style="width:100%">
      <h4>Classic</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>

  <div className="column people">
    <div className="content">
      <img src="/w3images/people1.jpg" alt="Car" style="width:100%">
      <h4>Girl</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column people">
    <div className="content">
    <img src="/w3images/people2.jpg" alt="Car" style="width:100%">
      <h4>Man</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
  <div className="column people">
    <div className="content">
    <img src="/w3images/people3.jpg" alt="Car" style="width:100%">
      <h4>Woman</h4>
      <p>Lorem ipsum dolor..</p>
    </div>
  </div>
<!-- END GRID -->
</div>

<!-- END MAIN -->
</div>



</body> */

filterSelection("all");
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("column");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// var btnContainer = document.getElementById("myBtnContainer");
// var btns = btnContainer.getElementsByClassName("btn");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function(){
//     var current = document.getElementsByClassName("active");
//     current[0].className = current[0].className.replace(" active", "");
//     this.className += " active";
//   });
// }

{
  /* <Gallerycolumn img={Mountain} name="Mountains" classNames="column "/>
<Gallerycolumn img={Lights} name="Lights"  classNames="column"/>
<Gallerycolumn img={Nature} name="Nature" classNames="column " />
<Gallerycolumn img={Cars1} name="Retro"  classNames="column "/>
<Gallerycolumn img={Cars2} name="Fast"  classNames="column"/>
<Gallerycolumn img={Cars3} name="Classic" className="column " />
<Gallerycolumn img={Girl} name="Girl"  classNames="column"/>
<Gallerycolumn img={Man} name="Man"  classNames="column " />
<Gallerycolumn img={Woman} name="Woman"  classNames="column "/> */
}
